import styled from "styled-components";

const Title = styled.h2`
  font-weight: 600;
`;

const PageTitle = ({ children }) => <Title>{children}</Title>;

export default PageTitle;
