import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { createGlobalStyle } from "styled-components";
import Home from "./containers/Home";
import OverView from "./containers/Overview";

const GlobalStyle = createGlobalStyle`
  html {
    --primary: #3657ff;
    --primary-hover: #506cff;
    --white: #ffffff;
    --gray: #eee;
    --black: #000;
    background-color: var(--white);
    font-size: 16px;
    font-family: 'Roboto', sans-serif;
  }

  body {
    margin: 0;
  }

  #root {
    height: 100vh;
    width: 80%;
    margin: 0 auto;
  }

`;

const PageRouter = () => (
  <Router>
    <Switch>
      <Route path="/" exact>
        <Home />
      </Route>
      <Route path="/overview/:employeeName">
        <OverView />
      </Route>
      <Redirect from="*" to="/" />
    </Switch>
  </Router>
);

const App = () => (
  <>
    <GlobalStyle whiteColor />
    <PageRouter /> {/* example of other top-level stuff */}
  </>
);

export default App;
