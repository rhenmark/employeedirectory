import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import PageTitle from "../../components/PageTitle";
import PageWrapper from "../../components/PageWrapper";
import SearchInput from "../../components/SearchInput";

const Home = () => {
  let history = useHistory();
  const [searchValue, setSearch] = useState("");

  const handleSearch = (event) => {
    event.preventDefault();
    history.push({
      pathname: `/overview/${searchValue}`,
    });
  };

  const handleChangeSearch = (event) => {
    setSearch(event.target.value);
  };

  return (
    <PageWrapper isHome>
      <PageTitle>Employee Explorer</PageTitle>
      <SearchInput
        onSearch={handleSearch}
        searchValue={searchValue}
        onChange={handleChangeSearch}
      />
    </PageWrapper>
  );
};

export default Home;
