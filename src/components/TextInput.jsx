import styled from "styled-components";

const Input = styled.input`
  width: 100%;
  height: 3.75rem;
  border: none;
  background-color: var(--gray);
  font-size: inherit;
  padding: 0 0.75rem;
  box-sizing: border-box;
`;

const TextInput = ({ onChange, value, placeholder, required }) => (
  <Input
    type="text"
    onChange={onChange}
    value={value}
    placeholder={placeholder}
    required={required}
  />
);

TextInput.defaultProps = {
  onChange: () => null,
  value: "",
  placeholder: "Type to search",
  required: false,
};

export default TextInput;
