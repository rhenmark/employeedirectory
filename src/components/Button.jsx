import styled from "styled-components";

const Btn = styled.button`
  width: 100%;
  height: 100%;
  background-color: var(--primary);
  color: var(--white);
  border: none;
  font-size: inherit;
  max-height: 3.75rem;
`;

const Button = ({ disabled, type, onClick, children }) => (
  <Btn disabled={disabled} type={type} onClick={onClick}>
    {children}
  </Btn>
);

Button.defaultProps = {
  disabled: false,
  type: "button",
  onClick: () => null,
};

export default Button;
