import styled from "styled-components";

const PageWrapper = styled.div`
  width: ${({ isHome }) => (isHome ? "50%" : "80%")};
  height: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export default PageWrapper;
