import { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import PageTitle from "../../components/PageTitle";
import PageWrapper from "../../components/PageWrapper";
import { fetchEmployee } from "../../api/get";
import styled from "styled-components";
import Button from "../../components/Button";

const OverViewContent = styled.div`
  width: 60%;
  justify-content: center;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const List = styled.ul`
  width: 100%;
  list-style: none;
  padding: 0;

  > li {
    padding: 0.75rem;
    font-size: 1.25rem;
    font-weight: 400;
    background-color: var(--gray);
    color: var(--black);
    margin-bottom: 0.125rem;
    text-transform: capitalize;
  }

  &.indirect {
    > li {
      font-size: 1.25rem;
      font-weight: 400;
      background-color: inherit;
      color: var(--black);
      margin-bottom: 6px;
      border-left: 4px solid #ddd;
      padding: 0.625rem 1rem;
      width: 100%;
      box-sizing: border-box;

      &:first-child {
        margin-top: 0.125rem;
      }
    }
  }
`;

const Empty = styled.div`
  width: 50%;

  > p {
    font-size: 1.25rem;
    text-align: center;
  }
`;

const EmptySub = ({ empty }) => {
  let history = useHistory();

  const handleGoBack = () => {
    history.replace("/");
  };

  return empty ? (
    <Empty>
      <p>No Subordinates found!</p>
      <Button onClick={handleGoBack}>Back to home</Button>
    </Empty>
  ) : null;
};

const ListItem = ({ subordinates }) =>
  subordinates.map((item) => {
    if (typeof item === "string") {
      return null;
    } else {
      return [...new Set(item["direct-subordinates"])].map((itemX) => (
        <li key={itemX}>
          {itemX}
          <IndirectSubordinates employeeName={itemX} />
        </li>
      ));
    }
  });

const IndirectSubordinates = ({ employeeName }) => {
  const [subordinates, setSubordinates] = useState([]);
  useEffect(() => {
    return fetchEmployee(employeeName).then((res) => {
      if (Array.isArray(res)) {
        setSubordinates(res);
      }
    });
  }, [employeeName]);

  return (
    <List className="indirect">
      <ListItem subordinates={subordinates} />
    </List>
  );
};

const OverView = () => {
  let { employeeName } = useParams();
  const [subordinates, setSubordinates] = useState([]);
  const [error, setError] = useState(false);

  useEffect(() => {
    return fetchEmployee(employeeName).then((res) => {
      if (Array.isArray(res)) {
        setSubordinates(res);
        setError(false);
      } else {
        setError(true);
      }
    });
  }, [employeeName]);

  return (
    <PageWrapper>
      <PageTitle>Employee Overview</PageTitle>
      <OverViewContent>
        <PageTitle>Subordinates of employee {employeeName}:</PageTitle>
        <EmptySub empty={error || subordinates.length <= 1} />
        <List>
          <ListItem subordinates={subordinates} />
        </List>
      </OverViewContent>
    </PageWrapper>
  );
};

export default OverView;
