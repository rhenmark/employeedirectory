import styled from "styled-components";
import TextInput from "./TextInput";
import Button from "./Button";

const SearchContainer = styled.form`
  width: 100%;
  height: auto;
  display: grid;
  grid-template-columns: minmax(60%, 75%) 1fr;
`;

const SearchInput = ({ searchValue, onSearch, onChange }) => {
  return (
    <SearchContainer onSubmit={onSearch}>
      <TextInput value={searchValue} onChange={onChange} required />
      <Button disabled={!searchValue} type="submit">
        Search
      </Button>
    </SearchContainer>
  );
};

export default SearchInput;
