export async function fetchEmployee(searchValue) {
  const response = await fetch(`${process.env.REACT_APP_API}/${searchValue}`);
  const data = await response.json();

  return data;
}
